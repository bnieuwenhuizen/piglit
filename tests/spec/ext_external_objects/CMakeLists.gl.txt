link_libraries (
	piglitutil_${piglit_target_api}
	${OPENGL_gl_LIBRARY}
)

piglit_add_executable (ext_external_objects-memory-object-api-errors
		       memory-object-api-errors.c)
piglit_add_executable (ext_external_objects-semaphore-api-errors
		       semaphore-api-errors.c)

IF(LIBVULKAN_FOUND)
	set(VK_BANDS_VERT vk_bands.vert.spv)
	set(VK_BANDS_FRAG vk_bands.frag.spv)
	set(VK_BANDS_VERT_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${VK_BANDS_VERT})
	set(VK_BANDS_FRAG_PATH ${CMAKE_CURRENT_SOURCE_DIR}/${VK_BANDS_FRAG})

	add_definitions(-DVK_BANDS_VERT="${VK_BANDS_VERT}")
	add_definitions(-DVK_BANDS_FRAG="${VK_BANDS_FRAG}")

	find_program(GLSLANG_VALIDATOR NAMES glslangValidator)

	IF(GLSLANG_VALIDATOR)
		add_custom_command(
			OUTPUT ${VK_BANDS_VERT_PATH}
			COMMAND ${GLSLANG_VALIDATOR} -V ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.vert -o ${VK_BANDS_VERT_PATH}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.vert
		)

		add_custom_command(
			OUTPUT ${VK_BANDS_FRAG_PATH}
			COMMAND ${GLSLANG_VALIDATOR} -V ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.frag -o ${VK_BANDS_FRAG_PATH}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.frag
		)
	ELSE()
		add_custom_command(
			OUTPUT ${VK_BANDS_VERT_PATH}
			COMMAND ${CMAKE_COMMAND} -E copy
			${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.vert.spv
			${VK_BANDS_VERT_PATH}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.vert
		)

		add_custom_command(
			OUTPUT ${VK_BANDS_FRAG_PATH}
			COMMAND ${CMAKE_COMMAND} -E copy
				${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.frag.spv
				${VK_BANDS_FRAG_PATH}
			DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/vk_bands.frag
		)
	ENDIF()

	include_directories(
		${GLEXT_INCLUDE_DIR}
		${OPENGL_INCLUDE_PATH}
	)

	link_libraries (
		piglitutil_${piglit_target_api}
		${OPENGL_gl_LIBRARY}
		${LIBVULKAN_LDFLAGS}
	)

	piglit_add_executable (ext_external_objects-vk-image-overwrite
		vk.c interop.c helpers.c vk_image_overwrite.c)
	piglit_add_executable (ext_external_objects-vk-image-display
		vk.c interop.c helpers.c vk_image_display.c ${VK_BANDS_VERT_PATH} ${VK_BANDS_FRAG_PATH})
	piglit_add_executable (ext_external_objects-vk-image-display-overwrite
		vk.c interop.c helpers.c vk_image_display_overwrite.c ${VK_BANDS_VERT_PATH} ${VK_BANDS_FRAG_PATH})
	piglit_add_executable (ext_external_objects-vk-buf-exchange
		vk.c interop.c helpers.c vk_buf_exchange.c ${VK_BANDS_VERT_PATH} ${VK_BANDS_FRAG_PATH})
ENDIF()

# vim: ft=cmake:
